﻿using DSM_AutomationFramework;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace DSM_AutomationTests
{
    [Binding]
    public sealed class Procedures
    {
        [BeforeTestRun]
        public static void BeforeRun()
        {
            File.Delete(LogPath);
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            Driver.Initialise();
            Driver.Instance.Manage().Window.Maximize();
        }

        [AfterStep]

        public void Check()
        {
            //var exception = ScenarioContext.Current.TestError;
            //if (exception is WebDriverException)
            //{
            //    LogError();
            //}

            //if (exception is SystemException)
            //{
            //    LogError();
            //}

            //if (exception is AssertionException)
            //{
            //    LogError();
            //}
        }

        [AfterScenario]
        public void AfterScenario()
        {
            Thread.Sleep(1000);
            //Driver.Close();
        }

        public static void AppendToFile(string path, string str)
        {
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.Write(str);
            }
        }

        public static void LogError()
        {
            //var exception = ScenarioContext.Current.TestError;

            AppendToFile(LogPath, FeatureContext.
               Current.FeatureInfo.Title + '\r' + '\n');
            AppendToFile(LogPath, ScenarioContext.
                Current.ScenarioInfo.Title + '\r' + '\n');
            string Error = ScenarioContext.
                Current.TestError.Message;
            string Step = ScenarioContext.
                Current.StepContext.StepInfo.Text;
            AppendToFile(LogPath, Step + '\r' + '\n');
            AppendToFile(LogPath, Error.ToString());
            AppendToFile(LogPath, " " + '\r' + '\n');
        }

        public static string Teardown           = Path.Combine(TestContext.CurrentContext.TestDirectory, ConfigurationManager.AppSettings["Teardown"]);
        public static string LogPath            = Path.Combine(TestContext.CurrentContext.TestDirectory, ConfigurationManager.AppSettings["LogPath"]);
        public static string TimePath           = Path.Combine(TestContext.CurrentContext.TestDirectory, ConfigurationManager.AppSettings["TimePath"]);
        public static string DBLogPath          = Path.Combine(TestContext.CurrentContext.TestDirectory, ConfigurationManager.AppSettings["DBLog"]);
        public static string ScreenPath         = Path.Combine(TestContext.CurrentContext.TestDirectory, ConfigurationManager.AppSettings["ScreenshotPath"]);
        public static string AllScreenPath      = Path.Combine(TestContext.CurrentContext.TestDirectory, ConfigurationManager.AppSettings["AllScreenshotsPath"]);
        public static string Document           = Path.Combine(TestContext.CurrentContext.TestDirectory, ConfigurationManager.AppSettings["ErrorDocPath"]);
        public static string ExcelSheetsPath    = Path.Combine(TestContext.CurrentContext.TestDirectory, ConfigurationManager.AppSettings["ExcelSheetsPath"]);
    }
}
