﻿using System;
using TechTalk.SpecFlow;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Interactions;
using DSM_AutomationFramework;

namespace DSM_AutomationTests
{
    [Binding]
    public class LoginSteps
    {
        [Given(@"I login as myself")]
        public void GivenILoginAsMyself()
        {
            LoginPage.LoginAsAaron();
        }

        [Given(@"I login as '(.*)'")]
        public void GivenILoginAs(string p0)
        {
            LoginPage.LoginAs(p0);
        }

        [Given(@"I login as User '(.*)' with Password '(.*)'")]
        public void GivenILoginAsUserWithPassword(string p0, string p1)
        {
            LoginPage.LoginAsUserWithPassword(p0, p1);
        }

        [Given(@"I change Password from '(.*)' to '(.*)'")]
        public void GivenIChangePasswordFromTo(string p0, string p1)
        {
            LoginPage.CreateNewPassword(p0, p1);
        }

        [When(@"I logout")]
        public void WhenILogout()
        {
            LoginPage.Logout();
        }

        [When(@"I click Save")]
        public void WhenIClickSave()
        {
            PageAction.ClickXPath(ChangePassword.Save);
        }

        [When(@"I click Signout")]
        public void WhenIClickSignout()
        {
            Check.WaitForXPath(Driver.Instance, ChangePassword.SignOut, 25);
            PageAction.ClickXPath(ChangePassword.SignOut);
        }

        [Then(@"I should see the login Menu")]
        public void ThenIShouldSeeTheLoginMenu()
        {
            Check.WaitForXPath(Driver.Instance,  Login.LoginGo, 25);
            Check.IfXPathElementIsThere(Login.LoginGo, "Error");
        }

    }
}
