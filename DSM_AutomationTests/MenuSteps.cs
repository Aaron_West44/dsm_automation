﻿using System;
using TechTalk.SpecFlow;
using DSM_AutomationFramework;

namespace DSM_AutomationTests
{
    [Binding]
    public class MenuSteps
    {
        [When(@"I goto My Workspace")]
        public void WhenIGotoMyWorkspace()
        {
            PageOps.GotoMyWorkSpace();
        }

        [When(@"I goto Reports")]
        public void WhenIGotoReports()
        {
            PageOps.GotoReports();
        }

        [Then(@"I should see the Report Grid")]
        public void ThenIShouldSeeTheReportGrid()
        {
            Check.IfXPathElementIsThere(Reports.GridBody, 
                "Reports Grid Not found");
        }


        [Given(@"I goto User Security")]
        [When(@"I goto User Security")]
        public void WhenIGotoUserSecurity()
        {
            PageOps.GotoUserSecurity();
        }

        //Because of 'And' after a 'Given' and before a 'When'
        //[Given(@"I goto User Security")]
        //public void GivenIGotoUserSecurity()
        //{
        //    PageOps.GotoUserSecurity();
        //}


        [Then(@"I should see the Users Grid")]
        public void ThenIShouldSeeTheUsersGrid()
        {
            Check.IfXPathElementIsThere(UserSecurity.GridBody, 
                "Users Grid Not found");
        }

        [When(@"I goto System Settings")]
        public void WhenIGotoSystemSettings()
        {
            PageOps.GotoSystemSettings();
        }

        [Then(@"I should see the System Settings Grid")]
        public void ThenIShouldSeeTheSystemSettingsGrid()
        {
            Check.IfXPathElementIsThere(SystemSettings.GridBody, 
                "System Settings Grid Not found");
        }

        [When(@"I goto Change Log")]
        public void WhenIGotoChangeLog()
        {
            PageOps.GotoChangeLog();
        }

        [Then(@"I should see the Change Log Grid")]
        public void ThenIShouldSeeTheChangeLogGrid()
        {
            Check.IfXPathElementIsThere(ChangeLog.GridBody,
                "Change Log Grid Not found");
        }

        [When(@"I goto Image Upload")]
        public void WhenIGotoImageUpload()
        {
            PageOps.GotoImageUpload();
        }

        [Then(@"I should see the Image Upload Input Panel")]
        public void ThenIShouldSeeTheImageUploadInputPanel()
        {
            Check.IfXPathElementIsThere(ImageUpload.SelectButton,
                "Image Upload Input Panel Not found");
        }

        [When(@"I goto Promotion Execution")]
        public void WhenIGotoPromotionExecution()
        {
            PageOps.GotoPromotionExecution();
        }

        [Then(@"I should see the Promotion Execution Calendar")]
        public void ThenIShouldSeeThePromotionExecutionCalendar()
        {
            Check.IfXPathElementIsThere(PromotionExecution.Calendar,
                "Calendar Not found");
        }

        [When(@"I goto On Shelf Availability")]
        public void WhenIGotoOnShelfAvailability()
        {
            PageOps.GotoOnShelfAvailability();
        }

        [Then(@"I should see the On Shelf Availability Grid")]
        public void ThenIShouldSeeTheOnShelfAvailabilityGrid()
        {
            Check.IfXPathElementIsThere(OnShelfAvailability.GridBody,
                "On Shelf Availability Grid Not found");
        }

        [When(@"I goto Item Transition")]
        public void WhenIGotoItemTransition()
        {
            PageOps.GotoItemTransition();
        }

        [Then(@"I should see the Item Transition Grid")]
        public void ThenIShouldSeeTheItemTransitionGrid()
        {
            Check.IfXPathElementIsThere(ItemTransition.Table,
                "Item Transition Grid Not found");
        }

        [When(@"I goto Report Subscriptions")]
        public void WhenIGotoReportSubscriptions()
        {
            PageOps.GotoReportSubscriptions();
        }

        [Then(@"I should see Subscriptions")]
        public void ThenIShouldSeeSubscriptions()
        {
            Check.IfXPathElementIsThere(ReportSubscriptions.Subscriptions,
                "Subscriptions Menu Item Not found");
        }

        [Then(@"I should see Report Groups")]
        public void ThenIShouldSeeReportGroups()
        {
            Check.IfXPathElementIsThere(ReportSubscriptions.ReportGroup,
                "Report Group Menu Item Not found");
        }

        [Then(@"I should see Report Designer")]
        public void ThenIShouldSeeReportDesigner()
        {
            Check.IfXPathElementIsThere(ReportSubscriptions.ReportDesigner,
                "Report Designer Menu Item Not found");
        }

        [When(@"I goto Cues")]
        public void WhenIGotoCues()
        {
            PageOps.GotoCues();
        }

        [Then(@"I should see the Cues Grid")]
        public void ThenIShouldSeeTheCuesGrid()
        {
            Check.IfXPathElementIsThere(Cues.GridBody,
                "Cues Grid Not found");
        }

        [When(@"I goto Item Groups")]
        public void WhenIGotoItemGroups()
        {
            PageOps.GotoItemGroups();
        }

        [Then(@"I should see the Item Groups Table")]
        public void ThenIShouldSeeTheItemGroupsTable()
        {
            Check.IfXPathElementIsThere(ItemGroups.Table,
                "Item Groups Table Not found");
        }

        [When(@"I goto Store Groups")]
        public void WhenIGotoStoreGroups()
        {
            PageOps.GotoStoreGroups();
        }

        [Then(@"I should see the Store Groups Grid")]
        public void ThenIShouldSeeTheStoreGroupsGrid()
        {
            Check.IfXPathElementIsThere(StoreGroups.Grid,
                "Store Groups Grid Not found");
        }

        [When(@"I goto Data Sources")]
        public void WhenIGotoDataSources()
        {
            PageOps.GotoDataSources();
        }

        [Then(@"I should see the Data Sources Grid")]
        public void ThenIShouldSeeTheDataSourcesGrid()
        {
            Check.IfXPathElementIsThere(DataSources.GridBody,
                "Data Sources Grid Not found");
        }

        [When(@"I goto Schiedule")]
        public void WhenIGotoSchiedule()
        {
            PageOps.GotoSchedule();
        }

        [Then(@"I should see the Schedule Grid")]
        public void ThenIShouldSeeTheScheduleGrid()
        {
            Check.IfXPathElementIsThere(Schedule.GridBody,
                "Schedule Grid Not found");
        }

        [When(@"I goto Upload Data")]
        public void WhenIGotoUploadData()
        {
            PageOps.GotoUploadData();
        }

        [Then(@"I should see the Upload Data Grid")]
        public void ThenIShouldSeeTheUploadDataGrid()
        {
            Check.IfXPathElementIsThere(UploadData.Grid,
                 "Upload Data Grid Not found");
        }

        [When(@"I goto Report Designer")]
        public void WhenIGotoReportDesigner()
        {
            PageOps.GotoReportDesigner();
        }

        [Then(@"I should see the Report Designer Dektop Panel")]
        public void ThenIShouldSeeTheReportDesignerDektopPanel()
        {
            //Check.IfXPathElementIsThere(ReportDesigner.DesktopPanel,
                 //"Report Designer Desktop Panel not found Not found");
        }

        [When(@"I goto the Main Menu")]
        public void WhenIGotoTheMainMenu()
        {
            PageOps.ClickMainMenu();
        }

        [Then(@"I should see the Settings Menu")]
        public void ThenIShouldSeeTheSettingsMenu()
        {
            Check.IfXPathElementIsThere(Menu.Settings, "Settings Menu not found");
        }

        [Then(@"I should see the Manage Menu")]
        public void ThenIShouldSeeTheManageMenu()
        {
            Check.IfXPathElementIsThere(Menu.Manage, "Manage Menu not found");
        }

        [Then(@"I should see the Data Menu")]
        public void ThenIShouldSeeTheDataMenu()
        {
            Check.IfXPathElementIsThere(Menu.Data, "Data Menu not found");
        }

        [When(@"I goto the Settings Menu")]
        public void WhenIGotoTheSettingsMenu()
        {
            PageOps.GotoSettings();
        }

        [When(@"I goto Settings")]
        public void WhenIGotoSettings()
        {
            PageOps.GotoSettings();
        }


        [Then(@"I should see Users")]
        public void ThenIShouldSeeUsers()
        {
            Check.IfXPathElementIsThere(Settings.Users, "Users not found");
        }

        [Then(@"I should see System Settings")]
        public void ThenIShouldSeeSystemSettings()
        {
            Check.IfXPathElementIsThere(Settings.SystemSettings, "SystemSettings not found");
        }
        [Then(@"I should see Change Log")]
        public void ThenIShouldSeeChangeLog()
        {
            Check.IfXPathElementIsThere(Settings.ChangeLog, "ChangeLog not found");
        }

        [Then(@"I should see Image Upload")]
        public void ThenIShouldSeeImageUpload()
        {
            Check.IfXPathElementIsThere(Settings.ImageUpload, "ImageUpload not found");
        }

        [When(@"I goto Manage")]
        public void WhenIGotoManage()
        {
            PageOps.GotoManage();
        }

        [Then(@"I should see Promotion Execution")]
        public void ThenIShouldSeePromotionExecution()
        {
            Check.IfXPathElementIsThere(Manage.PromotionExecution, "Promotion Execution not found");
        }

        [Then(@"I should see On Shelf Availability")]
        public void ThenIShouldSeeOnShelfAvailability()
        {
            Check.IfXPathElementIsThere(Manage.OnShelfAvailability, "On Shelf Availability not found");
        }
        [Then(@"I should see Item Transition")]
        public void ThenIShouldSeeItemTransition()
        {
            Check.IfXPathElementIsThere(Manage.ItemTransition, "Item Transition not found");
        }
        [Then(@"I should see Report Subscriptions")]
        public void ThenIShouldSeeReportSubscriptions()
        {
            Check.IfXPathElementIsThere(Manage.ReportSubscriptions, "Report Subscriptions  not found");
        }
        [Then(@"I should see Cues")]
        public void ThenIShouldSeeCues()
        {
            Check.IfXPathElementIsThere(Manage.Cues, "Cues  not found");
        }
        [Then(@"I should see Item Groups")]
        public void ThenIShouldSeeItemGroups()
        {
            Check.IfXPathElementIsThere(Manage.ItemGroups, "Item Groups not found");
        }
        [Then(@"I shouldsee Store Groups")]
        public void ThenIShouldseeStoreGroups()
        {
            Check.IfXPathElementIsThere(Manage.StoreGroups, "Store Groups not found");
        }


    }
}
