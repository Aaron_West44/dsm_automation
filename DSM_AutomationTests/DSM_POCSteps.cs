﻿using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using DSM_AutomationFramework;
using OpenQA.Selenium;
using TechTalk.SpecFlow.Assist;
using System.Linq;
using System.Threading;
using OpenQA.Selenium.Interactions;

namespace DSM_AutomationTests
{
    [Binding]
    public class DSM_POCSteps
    {

        [Given(@"select Retailer '(.*)'")]
        public void GivenSelectRetailer(string p0)
        {
            PageAction.SendKeysToXPath(DataSources.Retailer, (p0 + Keys.Enter));
            Check.WaitForXPath(Driver.Instance, DataSources.SaveButton, 10);
        }

        [When(@"select Retailer '(.*)'")]
        public void WhenSelectRetailer(string p0)
        {
            DataSourceOps.SelectRetailer(p0);
        }


        [Then(@"I should see Data Source Configuration")]
        public void ThenIShouldSeeDataSourceConfiguration(Table table)
        {
            Table singleRowTable = null;
            foreach (var row in table.Rows)
            {
                singleRowTable = new Table(table.Header.ToArray());
                singleRowTable.AddRow(row);
                var testData = singleRowTable.CreateInstance<CoreTestData>();

                string SwitchPath = DataSources.Switch;
                string SubjectAreaPath = DataSources.SubjectAreaColumn;

                SwitchPath = SwitchPath.Replace("ROWNO", testData.RowNo);
                SubjectAreaPath = SubjectAreaPath.Replace("ROWNO", testData.RowNo);

                var SwitchText = Driver.Instance.FindElement(By.XPath(SwitchPath)).Text;
                var SubjectText = Driver.Instance.FindElement(By.XPath(SubjectAreaPath)).Text;

                    Console.WriteLine("Expected: " 
                        + testData.Switch + " " 
                        + testData.SubjectArea
                        + '\r' + '\n'
                        + "Found: " + SwitchText + " " + SubjectText);

                    Procedures.AppendToFile(Procedures.LogPath, "Expected: " + '\t'
                        + testData.Switch + " "
                        + testData.SubjectArea + " "
                        + '\r');

                    Procedures.AppendToFile(Procedures.LogPath, "Found: " + '\t' + '\t'
                        + SwitchText + " "
                        + SubjectText + '\r');

                if (testData.Switch.Contains("On"))
                {
                    Check.IfXPathContainsText(SwitchPath, "On");
                }
                if (testData.Switch.Contains("Off"))
                {
                    Check.IfXPathContainsText(SwitchPath, "Off");
                }

                Assert.IsTrue(SubjectText.Contains(testData.SubjectArea.ToString()), "Subject Area Incorrect");
            }
        }

        [When(@"I enter New User")]
        public void WhenIEnterNewUser(Table table)
        {
            Table singleRowTable = null;
            foreach (var row in table.Rows)
            {
                singleRowTable = new Table(table.Header.ToArray());
                singleRowTable.AddRow(row);
                var testData = singleRowTable.CreateInstance<CoreTestData>();

                PageAction.ClickXPath       (UserSecurity.NewUser);

                Check.WaitForXPath(Driver.Instance, UserSecurity.Retailer, 10);
 
                Thread.Sleep(5000);

                PageAction.SendKeysToXPath  (UserSecurity.Username,         testData.Username);
                PageAction.ClickXPath       (UserSecurity.Email);
                PageAction.SendKeysToXPath  (UserSecurity.Email,            testData.Email);
                PageAction.ClickXPath       (UserSecurity.Email);
                Thread.Sleep(500);
                PageAction.ClickXPath       (UserSecurity.FirstName);
                PageAction.SendKeysToXPath  (UserSecurity.FirstName,        testData.FirstName);
                PageAction.SendKeysToXPath  (UserSecurity.LastName,         testData.LastName);
                PageAction.ClickXPath       (UserSecurity.Password);
                PageAction.SendKeysToXPath  (UserSecurity.Password,         testData.Password);
                PageAction.ClickXPath       (UserSecurity.ConfirmPassword);
                PageAction.SendKeysToXPath  (UserSecurity.ConfirmPassword,  testData.ConfirmPassword);
                PageAction.ClickXPath       (UserSecurity.Retailer);
                PageAction.SendKeysToXPath  (UserSecurity.Retailer,         testData.Retailer);
                PageAction.ClickXPath       (UserSecurity.Country);
                PageAction.SendKeysToXPath  (UserSecurity.Country,          testData.Country);
                PageAction.ClickXPath       (UserSecurity.Email);
                PageAction.SendKeysToXPath  (UserSecurity.Email, testData.Email);
                PageAction.ClickXPath       (UserSecurity.AddRetailer);

                Check.WaitForXPath          (Driver.Instance, UserSecurity.RetailerConfirmation, 5);

                PageAction.SendKeysToXPath  (UserSecurity.AddRetailer, Keys.Tab 
                    + Keys.Tab 
                    + Keys.Tab);

                PageAction.ClickXPath       (UserSecurity.Role);
                PageAction.SendKeysToXPath  (UserSecurity.Role,             testData.Role + Keys.Enter);

                ScenarioContext.Current["Username"] = (testData.Username.ToString());

                PageAction.ClickXPath(UserSecurity.Save);

                //Check.WaitUntilLoaded(25);
                Check.WaitForXPath(Driver.Instance, UserSecurity.GridFirstRow, 25);
            }
        }

        [When(@"Search for User")]
        public void WhenSearchForUser()
        {
            PageAction.SendKeysToXPath(UserSecurity.UserSearch, ScenarioContext.Current["Username"].ToString() + Keys.Enter);
        }
    }
}
