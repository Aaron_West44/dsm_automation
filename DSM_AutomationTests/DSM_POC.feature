﻿Feature: DSM_POC

Scenario:  ID_01: Goto Data Sources and check grid items for Walmart
	Given I login as 'aaron.west'
	When I goto Data Sources
	And select Retailer 'Walmart'
	Then I should see Data Source Configuration
	| RowNo | Switch | SubjectArea                                  |
	| 1     | Off    | ASM Distribution Load for Walmart            |
	| 2     | Off    | ASM Survey Load for Walmart                  |
	| 3     | On     | WMCR Store On Hand Adjustments               |
	| 4     | Off    | WMCR DRA                                     |
	| 5     | Off    | WMCR Markup/Markdown Events                  |
	| 6     | On     | WMCR Forecast                                |
	| 7     | Off    | WMCR Grocery PO                              |
	| 8     | On     | WMCR Sunday Company Summary Item-Level Facts |
	| 9     | On     | WMCR Auxiliary Item Master A9                |
	| 10    | On     | WMCR Item Master                             |
	| 11    | On     | WMCR Audit Data                              |
	| 12    | On     | WMCR Modular Map                             |
	| 13    | On     | WMCR Modular Master                          |
	| 14    | Off    | WMCR Daily Repl Instock - Category Level     |
	| 15    | Off    | WMCR Sunday Repl Instock - Category Level    |
	| 16    | Off    | WMCR Thursday Repl Instock - Category Level  |
	| 17    | Off    | WMCR Daily Repl Instock - Dept Level         |
	| 18    | Off    | WMCR Sunday Repl Instock - Dept Level        |
	| 19    | Off    | WMCR Thursday Repl Instock - Dept Level      |
	| 20    | Off    | WMCR Daily Repl Instock - Fineline Level     |
	| 21    | Off    | WMCR Sunday Repl Instock - Fineline Level    |
	| 22    | Off    | WMCR Thursday Repl Instock - Fineline Level  |
	| 23    | Off    | WMCR Daily Repl Instock - Item Level         |
	| 24    | Off    | WMCR Sunday Repl Instock - Item Level        |
	| 25    | Off    | WMCR Thursday Repl Instock - Item Level      |
	| 26    | On     | WMCR Comp Store                              |
	| 27    | Off    | WMCR Store DD Last Week                      |
	| 28    | On     | WMCR Store Formats                           |
	| 29    | On     | WMCR Daily Inventory                         |
	| 30    | Off    | WMCR Weekly Inventory                        |
	| 31    | Off    | WMCR Aux Store Master                        |
	| 32    | On     | WMCR Store Master                            |
	| 33    | On     | WMCR Daily Market Basket                     |
	| 34    | Off    | WMCR Weekly Market Basket                    |
	| 35    | On     | WMCR Saturday POS                            |
	| 36    | Off    | WMCR Daily POS                               |
	| 37    | On     | WMCR Sunday POS                              |
	| 38    | On     | WMCR Thursday POS                            |
	| 39    | Off    | WMCR Weekly POS                              |
	| 40    | Off    | WMCR US Stores Geocode or LatLong            |
	| 41    | On     | WMCR Store Traits                            |
	| 42    | On     | WMCR TVI Last Week                           |
	| 43    | On     | WMCR TVI Restate                             |
	| 44    | Off    | WMCR Daily SVIST                             |
	| 45    | Off    | WMCR Total Customer Returns                  |
	| 46    | Off    | Walmart US Sunday Variable UOM Sales         |
	| 47    | Off    | Walmart US Thursday Variable UOM Sales       |
	| 48    | Off    | Walmart US Variable UOM Sales                |
	| 49    | On     | WMCR Whse Align                              |
	| 50    | On     | WMCR Daily Whse Inventory                    |
	| 51    | Off    | WMCR Weekly Whse Inventory                   |
	| 52    | On     | WMCR Whse Master                             |
	| 53    | On     | WMCR Whse PO                                 |
	| 54    | On     | WMCR Daily Whse Ships & Receipts             |
	| 55    | Off    | WMCR Weekly Whse Ships & Receipts            |

Scenario:  ID_02: Create New User
	Given I login as 'aaron.west'
	And I goto User Security
	When I enter New User
	| Username | Email         | FirstName | LastName    | Password   | ConfirmPassword | Retailer | Country | Role  |
	| Test31   | Test@Test.com | Test31    | UserSurname | Password1! | Password1!      | Walmart  | United  | Power |
	And Search for User




