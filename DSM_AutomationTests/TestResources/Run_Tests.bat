echo off
cls
RMDIR "..\..\results" /S /Q
MD "..\..\results"
cd "..\..\packages\NUnit.ConsoleRunner.3.8.0\tools"

nunit3-console.exe "..\..\..\DSM_AutomationTests\bin\Debug\DSM_AutomationTests.dll"

	"..\..\..\packages\Pickles.CommandLine.2.18.2\tools\pickles.exe" --feature-directory="..\..\..\DSM_AutomationTests" --output-directory="..\..\..\results" --documentation-format=dHTML --link-results-file=".\TestResult.xml" --test-results-format=nunit3

	pause
	exit