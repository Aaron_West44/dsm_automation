echo off
cls

cd "..\..\packages\NUnit.ConsoleRunner.3.8.0\tools"

	"..\..\..\packages\Pickles.CommandLine.2.18.2\tools\pickles.exe" --feature-directory="..\..\..\DSM_AutomationTests" --output-directory="..\..\..\results" --documentation-format=dHTML --link-results-file=".\TestResult.xml" --test-results-format=nunit3

	pause
	exit
	