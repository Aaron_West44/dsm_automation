﻿Feature: Menu
	As an automation Tester
	I want a feature to be able to navigate to and test various menus within the application
	So that I can verify menu configuration for users and navigate accordingly

Scenario: ID_01: Goto My Workspace
	Given I login as myself
	When I goto My Workspace

Scenario: ID_02: Goto Reports
	Given I login as myself
	When I goto Reports
	Then I should see the Report Grid

Scenario: ID_03: Goto User Security
	Given I login as myself
	When I goto User Security
	Then I should see the Users Grid

Scenario: ID_04: Goto System Settings
	Given I login as 'aaron.west'
	When I goto System Settings
	Then I should see the System Settings Grid

Scenario: ID_05: Goto Change Log
	Given I login as 'aaron.west'
	When I goto Change Log
	Then I should see the Change Log Grid

Scenario: ID_06: Goto Image Upload
	Given I login as User 'aaron.west' with Password '!=4u2cOK'
	When I goto Image Upload
	Then I should see the Image Upload Input Panel

Scenario: ID_07: Goto Promotion Execution
	Given I login as myself
	When I goto Promotion Execution
	Then I should see the Promotion Execution Calendar

Scenario: ID_08: Goto On Shelf Availability
	Given I login as myself
	When I goto On Shelf Availability
	Then I should see the On Shelf Availability Grid

Scenario: ID_09: Goto Item Transition
	Given I login as myself
	When I goto Item Transition
	Then I should see the Item Transition Grid

Scenario: ID_10: Goto Report Subscriptions
	Given I login as myself
	When I goto Report Subscriptions
	Then I should see Subscriptions
	And I should see Report Groups
	And I should see Report Designer

Scenario: ID_11: Goto Cues
	Given I login as myself
	When I goto Cues
	Then I should see the Cues Grid

Scenario: ID_12: Goto Item Groups
	Given I login as myself
	When I goto Item Groups
	Then I should see the Item Groups Table

Scenario: ID_13: Goto Store Groups
	Given I login as myself
	When I goto Store Groups
	Then I should see the Store Groups Grid

Scenario:  ID_14: Goto Data Sources
	Given I login as myself
	When I goto Data Sources
	Then I should see the Data Sources Grid

Scenario: ID_15: Goto Schedule
	Given I login as myself
	When I goto Schiedule
	Then I should see the Schedule Grid

Scenario: ID_16 Goto Upload Data
	Given I login as myself
	When I goto Upload Data
	Then I should see the Upload Data Grid

Scenario: ID_17: Goto Report Designer
	Given I login as myself
	When I goto Report Designer
	Then I should see the Report Designer Dektop Panel

Scenario: ID_18: Check Main Menu Options
	Given I login as myself
	When I goto the Main Menu
	Then I should see the Settings Menu
	And I should see the Manage Menu
	And I should see the Data Menu

Scenario: ID_19: Check Settings Menu Options
	Given I login as myself
	When I goto Settings
	Then I should see Users
	And I should see System Settings
	And I should see Change Log
	And I should see Image Upload

Scenario: ID_20: Check Manage Menu Options
	Given I login as myself
	When I goto Manage
	Then I should see Promotion Execution
	And I should see On Shelf Availability
	And I should see Item Transition
	And I should see Report Subscriptions
	And I should see Cues
	And I should see Item Groups
	And I shouldsee Store Groups




