﻿Feature: Login
	As an automation Tester
	I want a feature to be able to login as various users
	So that I can enter the application in the required state

Scenario: ID_01: Login as myself
	Given I login as myself

Scenario: ID_02: Login as any User
	Given I login as 'aaron.west'

Scenario: ID_03: Login as any User and Logout
	Given I login as 'aaron.west'
	When I logout

Scenario: ID_04: Login as any User with any Password and Logout
	Given I login as User 'aaron.west' with Password '!=4u2cOK'
	When I logout

Scenario: ID_05: Change Password
	Given I login as User 'aaron.west' with Password '!=4u2cOK'
	And I change Password from 'Password1!' to 'Password2!'
	When I click Save
	And I click Signout
	Then I should see the login Menu