﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace DSM_AutomationFramework
{
    public class Check
    {
        public static bool WaitUntilLoaded(int timeoutInSeconds)
        {
            for (int i = 0; i < timeoutInSeconds; i++)
            {
                IWebDriver driver = Driver.Instance;
                if (IfXPathElementIsPresent(Objects.PolygonLoading) is true)
                    if (driver.FindElement(By.XPath(Objects.PolygonLoading)).Displayed
                        && driver.FindElement(By.XPath(Objects.PolygonLoading)).Enabled)
                    {
                        return false;
                    }

                Thread.Sleep(1000);
            }
            
            return true;
        }


        public static bool WaitForXPath(IWebDriver driver, string obj, int timeoutInSeconds)
        {
            for (int i = 0; i < timeoutInSeconds; i++)
            {
                if (IfXPathElementIsPresent(obj) is true)
                    if (driver.FindElement(By.XPath(obj)).Displayed 
                        && driver.FindElement(By.XPath(obj)).Enabled)
                    {
                        return true;
                    }

                Thread.Sleep(1000);
            }

            return false;
        }

        public static bool WaitForCss(IWebDriver driver, string obj, int timeoutInSeconds)
        {
            for (int i = 0; i < timeoutInSeconds; i++)
            {
                if (IfXPathElementIsPresent(obj) is true)
                    if (driver.FindElement(By.CssSelector(obj)).Displayed 
                        && driver.FindElement(By.CssSelector(obj)).Enabled)
                    {
                        return true;
                    }

                Thread.Sleep(1000);
            }

            return false;
        }

        public static bool IsCssElementPresent(string obj)
        {
            try
            {
                Driver.Instance.FindElement(By.CssSelector(obj));
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool IfXPathElementIsPresent(string obj)
        {
            try
            {
                Driver.Instance.FindElement(By.XPath(obj));
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void IfCssElementIsThere(string obj, string args)
        {
            Assert.IsTrue(IsCssElementPresent(obj), args);
            IWebElement element = Driver.Instance.FindElement(By.CssSelector(obj));
            Assert.IsTrue(element.Displayed && element.Enabled, args);
        }

        public static void IfCssElementIsNotThere(string obj, string args)
        {
            if (IsCssElementPresent(obj) is true)
            {
                IWebElement element = Driver.Instance.FindElement(By.CssSelector(obj));
                Assert.IsFalse(element.Displayed && element.Enabled, args);
            }
            else
            {
                Assert.IsFalse(IfXPathElementIsPresent(obj), args);
            }
        }

        public static void IfXPathElementIsThere(string obj, string args)
        {
            Assert.IsTrue(IfXPathElementIsPresent(obj), args);
            IWebElement element = Driver.Instance.FindElement(By.XPath(obj));
            Assert.IsTrue(element.Displayed && element.Enabled, args);
        }

        public static void IfXPathElementIsNotThere(string obj, string args)
        {
            if (IfXPathElementIsPresent(obj) is true)
            {
                IWebElement element = Driver.Instance.FindElement(By.XPath(obj));
                Assert.IsFalse(element.Displayed && element.Enabled, args);
            }
            else
            {
                Assert.IsFalse(IfXPathElementIsPresent(obj), args);
            }
        }

        public static void IfXPathContainsText(string obj, string text)
        {
            var Text = Driver.Instance.FindElement(By.XPath(obj)).Text;
            Assert.IsTrue(Text.Contains(text));
        }

        public static void IfCssContainsText(string obj, string text)
        {
            var Text = Driver.Instance.FindElement(By.CssSelector(obj)).Text;
            Assert.IsTrue(Text.Contains(text));
        }
    }


}
