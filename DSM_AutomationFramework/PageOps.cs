﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DSM_AutomationFramework
{
    public class PageOps
    {
        public static void ClickMainMenu()
        {
            PageAction.ClickXPath(Menu.MainMenu);
        }

        public static void GotoMyWorkSpace()
        {
            ClickMainMenu();
            PageAction.ClickXPath(Menu.MyWorkspace);
            PageAction.SendKeysToXPath(Menu.MyWorkspace, Keys.Escape);
        }

        public static void GotoReports()
        {
            ClickMainMenu();
            PageAction.ClickXPath(Menu.Reports);
            Check.WaitForXPath(Driver.Instance, Reports.GridBody, 25);
        }

        public static void GotoSettings()
        {
            ClickMainMenu();
            Thread.Sleep(150);
            PageAction.ClickXPath(Menu.Settings);
        }

        public static void GotoUserSecurity()
        {
            GotoSettings();
            PageAction.ClickXPath(Settings.Users);
            Check.WaitForXPath(Driver.Instance, UserSecurity.GridBody, 25);
        }

        public static void GotoSystemSettings()
        {
            GotoSettings();
            PageAction.ClickXPath(Settings.SystemSettings);
            Check.WaitForXPath(Driver.Instance, SystemSettings.GridBody, 25);
        }

        public static void GotoChangeLog()
        {
            GotoSettings();
            PageAction.ClickXPath(Settings.ChangeLog);
            Check.WaitForXPath(Driver.Instance, ChangeLog.GridBody, 25);
        }

        public static void GotoImageUpload()
        {
            GotoSettings();
            PageAction.ClickXPath(Settings.ImageUpload);
            Check.WaitForXPath(Driver.Instance, ImageUpload.SelectButton, 25);
        }

        public static void GotoManage()
        {
            ClickMainMenu();
            Thread.Sleep(150);
            PageAction.ClickXPath(Menu.Manage);
        }

        public static void GotoPromotionExecution()
        {
            GotoManage();
            PageAction.ClickXPath(Manage.PromotionExecution);
            Check.WaitForXPath(Driver.Instance, PromotionExecution.Calendar, 25);
        }

        public static void GotoOnShelfAvailability()
        {
            GotoManage();
            PageAction.ClickXPath(Manage.OnShelfAvailability);
            Check.WaitForXPath(Driver.Instance, OnShelfAvailability.GridBody, 25);
        }

        public static void GotoItemTransition()
        {
            GotoManage();
            PageAction.ClickXPath(Manage.ItemTransition);
            Check.WaitForXPath(Driver.Instance, ItemTransition.Table, 25);
        }

        public static void GotoReportSubscriptions()
        {
            GotoManage();
            PageAction.ClickXPath(Manage.ReportSubscriptions);
            Check.WaitForXPath(Driver.Instance, ReportSubscriptions.ReportDesigner, 25);
        }

        public static void GotoCues()
        {
            GotoManage();
            PageAction.ClickXPath(Manage.Cues);
            Check.WaitForXPath(Driver.Instance, Cues.GridBody, 25);
        }

        public static void GotoItemGroups()
        {
            GotoManage();
            PageAction.ClickXPath(Manage.ItemGroups);
            Check.WaitForXPath(Driver.Instance, ItemGroups.Table, 25);
        }

        public static void GotoStoreGroups()
        {
            GotoManage();
            PageAction.ClickXPath(Manage.StoreGroups);
            Check.WaitForXPath(Driver.Instance, StoreGroups.Grid, 25);
        }

        public static void GotoData()
        {
            ClickMainMenu();
            Thread.Sleep(150);
            PageAction.ClickXPath(Menu.Data);
        }

        public static void GotoDataSources()
        {
            GotoData();
            PageAction.ClickXPath(Data.DataSources);
            Check.WaitForXPath(Driver.Instance, DataSources.SaveButton, 25);
        }

        public static void GotoSchedule()
        {
            GotoData();
            PageAction.ClickXPath(Data.Schedule);
            Check.WaitForXPath(Driver.Instance, Schedule.GridBody, 25);
        }

        public static void GotoUploadData()
        {
            GotoData();
            PageAction.ClickXPath(Data.UploadData);
            Check.WaitForXPath(Driver.Instance, UploadData.Grid, 25);
        }

        public static void GotoMonitorDataLoads()
        {
            GotoData();
            PageAction.ClickXPath(Data.MonitorDataLoads);
        }

        public static void GotoReportDesigner()
        {
            ClickMainMenu();
            PageAction.ClickXPath(Menu.ReportDesigner);
        }
    }

    public class DataSourceOps
    {
        public static void SelectRetailer(string retailer)
        {
            PageAction.ClickXPath(DataSources.Retailer);
            PageAction.SendKeysToXPath(DataSources.Retailer, (retailer + Keys.Enter));
            Check.WaitForXPath(Driver.Instance, DataSources.SaveButton, 10);
        }
    }
}
