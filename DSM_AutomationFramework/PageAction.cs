﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DSM_AutomationFramework
{
    public class PageAction
    {
        public static void SendKeysToCss(string obj, string keys)
        {
            Driver.Instance.FindElement
                (By.CssSelector(obj)).SendKeys(keys);
            Thread.Sleep(150);
        }

        public static void SendKeysToXPath(string obj, string keys)
        {
            Driver.Instance.FindElement
                (By.XPath(obj)).SendKeys(keys);
            Thread.Sleep(150);
        }

        public static void ClickCss(string obj)
        {
            Driver.Instance.FindElement
                (By.CssSelector(obj)).Click();
            Thread.Sleep(150);
        }

        public static void ClickXPath(string obj)
        {
            Driver.Instance.FindElement
                (By.XPath(obj)).Click();
            Thread.Sleep(150);
        }

        public static void SwitchWindow()
        {
            Driver.Instance.SwitchTo().Window
            (Driver.Instance.WindowHandles.Last());
            Thread.Sleep(250);
        }
    }
}
