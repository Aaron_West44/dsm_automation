﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DSM_AutomationFramework
{
    public class LoginPage
    {
        public static void GotoLoginPage()
        {
            string Homepage = ConfigurationManager.AppSettings["Home"];
            Driver.Instance.Navigate().GoToUrl(Homepage);
        }

        public static LoginCommand LoginToSystemAs(string username)
        {
            return new LoginCommand(username);
        }


        public static void CreateNewPassword(string oldpass, string newpass)
        {
            Check.WaitForXPath(Driver.Instance, ChangePassword.Cancel, 25);
            PageAction.SendKeysToXPath(ChangePassword.OldPassword, oldpass);
            PageAction.SendKeysToXPath(ChangePassword.NewPassword, newpass);
            PageAction.SendKeysToXPath(ChangePassword.ConfirmPassword, newpass);
        }

        public static void AcceptCookies()
        {
            PageAction.ClickCss(Login.AcceptCookies);
        }

        public static void LoginAsAaron()
        {
            GotoLoginPage();
            //AcceptCookies();
            Thread.Sleep(500);
            LoginToSystemAs("aaron.west")
                .WithPassword("!=4u2cOK")
                .LoginToSystem();

            //The above, hard coded login will eventually be turned into a SQL query for a user type if required.  
            //Here is just an example of how this can be done...

            //using (SqlConnection Con = new SqlConnection
            //    (ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString))
            //{
            //    SqlCommand user = new SqlCommand(User.NewUser, Con);

            //    Con.Open();
            //    string username = user.ExecuteScalar() as string;
            //    Console.WriteLine(username);
            //    LoginToSystemAs(username)
            //        .WithPassword("!=4u2cOK")
            //        .LoginToSystem();
            //}
        }

        public static void LoginAs(string username)
        {
            GotoLoginPage();
            AcceptCookies();
            Thread.Sleep(500);
            LoginToSystemAs(username)
                .WithPassword("!=4u2cOK")
                .LoginToSystem();
        }

        public static void LoginAsUserWithPassword(string username, string password)
        {
            GotoLoginPage();
            AcceptCookies();
            Thread.Sleep(500);
            LoginToSystemAs(username)
                .WithPassword(password)
                .LoginToSystem();
        }

        public static void Logout()
        {
            PageAction.ClickXPath(UserOptions.User);
            PageAction.ClickCss(UserOptions.Logout);
        }
    }
    
    public class LoginCommand
    {
        private readonly string userName;
        private string password;

        public LoginCommand(string userName)
        {
            this.userName = userName;
        }

        public LoginCommand WithPassword(string password)
        {
            this.password = password;
            return this;
        }

        public void LoginToSystem()
        {
            PageAction.SendKeysToCss(Login.Username, userName);
            PageAction.SendKeysToCss(Login.Password, password);
            PageAction.ClickCss(Login.LoginGo);
            Check.WaitForXPath(Driver.Instance, UserOptions.User, 20);
        }
    }
}
