﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSM_AutomationFramework
{
    public class Driver
    {
        private static IWebDriver _webDriver;

        public static IWebDriver Instance
        {
            get
            {
                return _webDriver ?? Create();
            }
        }

        private static IWebDriver Create()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--lang=en-US");

            _webDriver = new ChromeDriver(options);
            return _webDriver;
        }

        public static void Initialise()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--lang=en-US");
            _webDriver = new ChromeDriver(options);
        }

        public static void Close()
        {
            if (_webDriver == null) return;
            _webDriver.Close();
        }
    }
}

