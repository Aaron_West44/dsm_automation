﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSM_AutomationFramework
{
    public class CoreTestData
    {
        public string RowNo { get; set; }
        public string Switch { get; set; }
        public string SubjectArea { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Retailer { get; set; }
        public string Country { get; set; }
        public string Role { get; set; }
    }
}
