﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSM_AutomationFramework
{
    public class Objects
    {
        //public const string PolygonLoading =  "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div[3]/div[1]/div[2]/div/div[1]/div/div/svg/g/polygon[2]";
        public static string PolygonLoading =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[1]/div/div/svg/g/polygon[*]";
    }

    public class Login
    {
        public const string AcceptCookies   =   "#btnAccept";
        //public const string Username        =   "#ContentPlaceHolder1_UsernameTextBox";
        public const string Username        =   "#username";
        //public const string Password        =   "#ContentPlaceHolder1_PasswordTextBox";
        public const string Password        =   "#password";
        //public const string LoginGo         =   "#ContentPlaceHolder1_SubmitButton";
        public const string LoginGo         =   "#submit";
    }

    public class UserOptions
    {
        public const string User                =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[2]/span[2]/a/span/span[1]";
        public const string Logout              =   "body > ul > li:nth-child(2) > ul > li:nth-child(3) > a";
    }

    public class ChangePassword
    {
        public const string Cancel              =   "//*[@id=\"ng-app\"]/body/div[3]/div[2]/div/button[1]";
        public const string OldPassword         =   "//*[@id=\"ng-app\"]/body/div[3]/div[1]/div/div/div[2]/form/div/div[2]/input";
        public const string NewPassword         =   "//*[@id=\"ng-app\"]/body/div[3]/div[1]/div/div/div[2]/form/div/div[3]/input";
        public const string ConfirmPassword     =   "//*[@id=\"ng-app\"]/body/div[3]/div[1]/div/div/div[2]/form/div/div[4]/input";
        public const string Save                =   "//*[@id=\"ng-app\"]/body/div[3]/div[2]/div/button[2]";
        public const string SignOut             =   "//*[@id=\"ng-app\"]/body/div[3]/div/div/div/div[2]/button";
    }

    public class Menu
    {
        public const string MainMenu            =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/button";
        public const string MyWorkspace         =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/div/div/ul/li[1]/a[contains(@title,'My Workspace')]";
        public const string Reports             =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/div/div/ul/li[2]/a[contains(@title,'Reports')]";
        public const string Settings            =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/div/div/ul/li[3]/button";
        public const string Manage              =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/div/div/ul/li[4]/button";
        public const string Data                =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/div/div/ul/li[5]/button";
        public const string ReportDesigner      =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/div/div/ul/li[6]/a";
    }

    public class Settings
    {
        //public const string Users             =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/section/div/ul[1]/li/ul/li[1]/a";
        //public const string SystemSettings    =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/section/div/ul[1]/li/ul/li[2]/a";
        //public const string ChangeLog         =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/section/div/ul[1]/li/ul/li[3]/a";
        //public const string ImageUpload       =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/section/div/ul[2]/li/ul/li/a";

        public const string Users               =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div/ul[*]/li/ul/li[*]/a[contains(@name,'users')]";
        public const string SystemSettings      =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div/ul[*]/li/ul/li[*]/a[contains(@name,'systemsettings')]";
        public const string ChangeLog           =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div/ul[*]/li/ul/li[*]/a[contains(@name,'changelog')]";
        public const string ImageUpload         =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div/ul[*]/li/ul/li[*]/a[contains(@name,'imageupload')]";
    }

    public class Manage
    {
        public const string PromotionExecution  =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div[*]/ul/li/ul/li[*]/a[contains(@title,'Promotion Execution')]";
        public const string OnShelfAvailability =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div[*]/ul/li/ul/li[*]/a[contains(@title,'On Shelf Availability')]";
        public const string ItemTransition      =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div[*]/ul/li/ul/li[*]/a[contains(@title,'Item Transition')]";
        public const string ReportSubscriptions =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div[*]/ul/li/ul/li[*]/a[contains(@title,'Report Subscriptions')]";
        public const string Cues                =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div[*]/ul/li/ul/li[*]/a[contains(@title,'Cues')]";
        public const string ItemGroups          =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div[*]/ul/li/ul/li[*]/a[contains(@title,'Item Groups')]";
        public const string StoreGroups         =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[*]/nav/div/section/div[*]/ul/li/ul/li[*]/a[contains(@title,'Store Groups')]";
    }

    public class Data
    {
        public const string DataSources         =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/section/div/ul[1]/li/ul/li[1]/a";
        public const string Schedule            =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/section/div/ul[1]/li/ul/li[2]/a";
        public const string UploadData          =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/section/div/ul[1]/li/ul/li[3]/a";
        public const string MonitorDataLoads    =   "//*[@id=\"ng-app\"]/body/div[1]/div[1]/header/div[3]/nav/div/section/div/ul[2]/li/ul/li/a";
    }

    public class Reports
    {
        public const string GridBody            =   "//*[@id=\"report-group-464\"]/div[2]/div/div/table/tbody";
    }

    public class UserSecurity
    {
        public const string PageBody            =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]";
        //public const string GridBody            =   "//*[@id=\"userTable\"]/tbody"; //Grid from old version
        public const string GridBody            =   "//*[@id=\"userGroupTable\"]/tbody";
        public const string GridFirstRow        =   "//*[@id=\"userTable\"]/tbody/tr[1]";
        public const string UserSearch          =   "//*[@id=\"txt-search\"]";
        public const string NewUser             =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div[3]/div[3]/a";
        public const string Username            =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[1]/div[1]/div[1]/div/input";
        public const string Email               =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[1]/div[2]/div[1]/div/input";
        public const string FirstName           =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[2]/div[1]/div[1]/div/input";
        public const string LastName            =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[2]/div[2]/div[1]/div/input";
        public const string Password            =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[3]/div[1]/div[1]/div/input";
        public const string ConfirmPassword     =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[3]/div[2]/div[1]/input";
        public const string Retailer            =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[5]/section/div/div[1]/div[1]/div/select";
        public const string AddRetailer         =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[5]/section/div/div[1]/div[3]/div[2]/a";
        public const string RetailerConfirmation=   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[5]/section/div/div[2]/div[1]/div";
        public const string Country             =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[5]/section/div/div[1]/div[2]/div/select";
        public const string ReportServerAcces   =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[6]/section/div[1]/div[1]/label/span[1]";
        public const string RolesSection        =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[7]/section";
        public const string BottomSection       =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[7]";
        public const string OrchestroRoles      =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[1]/div[2]/form/div[2]/div[7]/section/div[1]/div/label";
        public const string Role                =   "//*[@id=\"userroles\"]/ul/li/input";
        public const string Save                =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/span/span/button";
        public const string Cancel              =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div/div/div[2]/div[3]/button";
    }

    public class SystemSettings
    {
        public const string GridBody            =   "//*[@id=\"tableFtpServer\"]/tbody";
    }

    public class ChangeLog
    {
        public const string GridBody            =   "//*[@id=\"reportTable\"]/tbody";
    }

    public class ImageUpload
    {
        public const string InputPanel          =   "//*[@id=\"imageUploadDiv\"]/input";
        public const string SelectButton        =   "//*[@id=\"selectFilesButton\"]";
    }

    public class PromotionExecution
    {
        public const string Calendar            =   "//*[@id=\"grid-calendar\"]";
    }

    public class OnShelfAvailability
    {
        public const string GridBody            =   "//*[@id=\"grid-example-1\"]/div/table/tbody";
    }

    public class ItemTransition
    {
        public const string GridBody            =   "//*[@id=\"grid-example-1\"]/div/table/tbody";
        public const string Table               =   "//*[@id=\"grid-example-1\"]/div/table";
    }

    public class ReportSubscriptions
    {
        public const string PageTitle           =   "//*[@id=\"pageMessage\"]";
        public const string Subscriptions       =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div/div/ul/li[1]/span/a";
        public const string ReportGroup         =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div/div/ul/li[2]/span/a";
        public const string ReportDesigner      =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div[1]/div/div/div/div[1]/div/div/div/ul/li[3]/span/a";
    }

    public class Cues
    {
        public const string GridBody            =   "//*[@id=\"grid-example-1\"]/div/table/tbody";
    }

    public class ItemGroups
    {
        public const string Table               =   "//*[@id=\"grid-example-1\"]/div/table";
    }

    public class StoreGroups
    {
        public const string Grid                =   "//*[@id=\"grid-example-1\"]";
    }

    public class DataSources
    {
        public const string SaveButton          =   "//*[@id=\"ng-app\"]/body/div[1]/div[2]/div[2]/div[2]/div[2]/div[2]/div/div[2]/div[2]/div/form/div[5]/div/button[1]";
        public const string GridBody            =   "//*[@id=\"tableDataSources\"]/tbody";
        public const string Switch              =   "//*[@id=\"tableDataSources\"]/tbody/tr[ROWNO]/td[1]";
        public const string SubjectAreaColumn   =   "//*[@id=\"tableDataSources\"]/tbody/tr[ROWNO]/td[2]";
        public const string Retailer            =   "//*[@id=\"select-retailer-supplier\"]/div/select";
    }

    public class Schedule
    {
        public const string GridBody            =   "//*[@id=\"timeTable\"]/tbody";
    }

    public class UploadData
    {
        public const string Grid                =   "//*[@id=\"grid-data-upload\"]";
    }

    public class ReportDesigner
    {
        public const string DesktopPanel        =   "//*[@id=\"dktpSectionView\"]";
    }
}
