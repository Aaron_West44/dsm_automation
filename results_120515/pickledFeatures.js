jsonPWrapper ({
  "Features": [
    {
      "RelativeFolder": "DSM_POC.feature",
      "Feature": {
        "Name": "DSM_POC",
        "Description": "",
        "FeatureElements": [
          {
            "Name": "ID_01: Goto Data Sources and check grid items for Walmart",
            "Slug": "id01-goto-data-sources-and-check-grid-items-for-walmart",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as 'aaron.west'",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Data Sources",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "select Retailer 'Walmart'",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see Data Source Configuration",
                "TableArgument": {
                  "HeaderRow": [
                    "RowNo",
                    "Switch",
                    "SubjectArea"
                  ],
                  "DataRows": [
                    [
                      "1",
                      "Off",
                      "ASM Distribution Load for Walmart"
                    ],
                    [
                      "2",
                      "Off",
                      "ASM Survey Load for Walmart"
                    ],
                    [
                      "3",
                      "On",
                      "WMCR Store On Hand Adjustments"
                    ],
                    [
                      "4",
                      "Off",
                      "WMCR DRA"
                    ],
                    [
                      "5",
                      "Off",
                      "WMCR Markup/Markdown Events"
                    ],
                    [
                      "6",
                      "On",
                      "WMCR Forecast"
                    ],
                    [
                      "7",
                      "Off",
                      "WMCR Grocery PO"
                    ],
                    [
                      "8",
                      "Off",
                      "WMCR Sunday Company Summary Item-Level Facts"
                    ],
                    [
                      "9",
                      "On",
                      "WMCR Auxiliary Item Master A9"
                    ],
                    [
                      "10",
                      "On",
                      "WMCR Item Master"
                    ],
                    [
                      "11",
                      "On",
                      "WMCR Audit Data"
                    ],
                    [
                      "12",
                      "On",
                      "WMCR Modular Map"
                    ],
                    [
                      "13",
                      "On",
                      "WMCR Modular Master"
                    ],
                    [
                      "14",
                      "Off",
                      "WMCR Daily Repl Instock - Category Level"
                    ],
                    [
                      "15",
                      "Off",
                      "WMCR Sunday Repl Instock - Category Level"
                    ],
                    [
                      "16",
                      "Off",
                      "WMCR Thursday Repl Instock - Category Level"
                    ],
                    [
                      "17",
                      "Off",
                      "WMCR Daily Repl Instock - Dept Level"
                    ],
                    [
                      "18",
                      "Off",
                      "WMCR Sunday Repl Instock - Dept Level"
                    ],
                    [
                      "19",
                      "Off",
                      "WMCR Thursday Repl Instock - Dept Level"
                    ],
                    [
                      "20",
                      "Off",
                      "WMCR Daily Repl Instock - Fineline Level"
                    ],
                    [
                      "21",
                      "Off",
                      "WMCR Sunday Repl Instock - Fineline Level"
                    ],
                    [
                      "22",
                      "Off",
                      "WMCR Thursday Repl Instock - Fineline Level"
                    ],
                    [
                      "23",
                      "Off",
                      "WMCR Daily Repl Instock - Item Level"
                    ],
                    [
                      "24",
                      "Off",
                      "WMCR Sunday Repl Instock - Item Level"
                    ],
                    [
                      "25",
                      "Off",
                      "WMCR Thursday Repl Instock - Item Level"
                    ],
                    [
                      "26",
                      "On",
                      "WMCR Comp Store"
                    ],
                    [
                      "27",
                      "Off",
                      "WMCR Store DD Last Week"
                    ],
                    [
                      "28",
                      "On",
                      "WMCR Store Formats"
                    ],
                    [
                      "29",
                      "On",
                      "WMCR Daily Inventory"
                    ],
                    [
                      "30",
                      "Off",
                      "WMCR Weekly Inventory"
                    ],
                    [
                      "31",
                      "Off",
                      "WMCR Aux Store Master"
                    ],
                    [
                      "32",
                      "On",
                      "WMCR Store Master"
                    ],
                    [
                      "33",
                      "On",
                      "WMCR Daily Market Basket"
                    ],
                    [
                      "34",
                      "Off",
                      "WMCR Weekly Market Basket"
                    ],
                    [
                      "35",
                      "On",
                      "WMCR Saturday POS"
                    ],
                    [
                      "36",
                      "Off",
                      "WMCR Daily POS"
                    ],
                    [
                      "37",
                      "On",
                      "WMCR Sunday POS"
                    ],
                    [
                      "38",
                      "On",
                      "WMCR Thursday POS"
                    ],
                    [
                      "39",
                      "Off",
                      "WMCR Weekly POS"
                    ],
                    [
                      "40",
                      "Off",
                      "WMCR US Stores Geocode or LatLong"
                    ],
                    [
                      "41",
                      "On",
                      "WMCR Store Traits"
                    ],
                    [
                      "42",
                      "On",
                      "WMCR TVI Last Week"
                    ],
                    [
                      "43",
                      "On",
                      "WMCR TVI Restate"
                    ],
                    [
                      "44",
                      "Off",
                      "WMCR Daily SVIST"
                    ],
                    [
                      "45",
                      "Off",
                      "WMCR Total Customer Returns"
                    ],
                    [
                      "46",
                      "Off",
                      "Walmart US Sunday Variable UOM Sales"
                    ],
                    [
                      "47",
                      "Off",
                      "Walmart US Thursday Variable UOM Sales"
                    ],
                    [
                      "48",
                      "Off",
                      "Walmart US Variable UOM Sales"
                    ],
                    [
                      "49",
                      "On",
                      "WMCR Whse Align"
                    ],
                    [
                      "50",
                      "On",
                      "WMCR Daily Whse Inventory"
                    ],
                    [
                      "51",
                      "Off",
                      "WMCR Weekly Whse Inventory"
                    ],
                    [
                      "52",
                      "On",
                      "WMCR Whse Master"
                    ],
                    [
                      "53",
                      "On",
                      "WMCR Whse PO"
                    ],
                    [
                      "54",
                      "On",
                      "WMCR Daily Whse Ships & Receipts"
                    ],
                    [
                      "55",
                      "Off",
                      "WMCR Weekly Whse Ships & Receipts"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_02: Create New User",
            "Slug": "id02-create-new-user",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as 'aaron.west'",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I goto User Security",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I enter New User",
                "TableArgument": {
                  "HeaderRow": [
                    "Username",
                    "Email",
                    "FirstName",
                    "LastName",
                    "Password",
                    "ConfirmPassword",
                    "Retailer",
                    "Country",
                    "Role"
                  ],
                  "DataRows": [
                    [
                      "Test10",
                      "Test@Test.com",
                      "Test10",
                      "UserSurname",
                      "Password1!",
                      "Password1!",
                      "Walmart",
                      "United",
                      "Power"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "Search for User",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": false
            }
          }
        ],
        "Result": {
          "WasExecuted": true,
          "WasSuccessful": false
        },
        "Tags": []
      },
      "Result": {
        "WasExecuted": true,
        "WasSuccessful": false
      }
    },
    {
      "RelativeFolder": "Login.feature",
      "Feature": {
        "Name": "Login",
        "Description": "As an automation Tester\r\nI want a feature to be able to login as various users\r\nSo that I can enter the application in the required state",
        "FeatureElements": [
          {
            "Name": "ID_01: Login as myself",
            "Slug": "id01-login-as-myself",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_02: Login as any User",
            "Slug": "id02-login-as-any-user",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as 'aaron.west'",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_03: Login as any User and Logout",
            "Slug": "id03-login-as-any-user-and-logout",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as 'aaron.west'",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I logout",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_04: Login as any User with any Password and Logout",
            "Slug": "id04-login-as-any-user-with-any-password-and-logout",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as User 'aaron.west' with Password '!=4u2cOK'",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I logout",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          }
        ],
        "Result": {
          "WasExecuted": true,
          "WasSuccessful": true
        },
        "Tags": []
      },
      "Result": {
        "WasExecuted": true,
        "WasSuccessful": true
      }
    },
    {
      "RelativeFolder": "Menu.feature",
      "Feature": {
        "Name": "Menu",
        "Description": "As an automation Tester\r\nI want a feature to be able to navigate to and test various menus within the application\r\nSo that I can verify menu configuration for users and navigate accordingly",
        "FeatureElements": [
          {
            "Name": "ID_01: Goto My Workspace",
            "Slug": "id01-goto-my-workspace",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto My Workspace",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_02: Goto Reports",
            "Slug": "id02-goto-reports",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Reports",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Report Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_03: Goto User Security",
            "Slug": "id03-goto-user-security",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto User Security",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Users Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_04: Goto System Settings",
            "Slug": "id04-goto-system-settings",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as 'aaron.west'",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto System Settings",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the System Settings Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_05: Goto Change Log",
            "Slug": "id05-goto-change-log",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as 'aaron.west'",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Change Log",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Change Log Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_06: Goto Image Upload",
            "Slug": "id06-goto-image-upload",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as User 'aaron.west' with Password '!=4u2cOK'",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Image Upload",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Image Upload Input Panel",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_07: Goto Promotion Execution",
            "Slug": "id07-goto-promotion-execution",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Promotion Execution",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Promotion Execution Calendar",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_08: Goto On Shelf Availability",
            "Slug": "id08-goto-on-shelf-availability",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto On Shelf Availability",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the On Shelf Availability Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_09: Goto Item Transition",
            "Slug": "id09-goto-item-transition",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Item Transition",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Item Transition Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_10: Goto Report Subscriptions",
            "Slug": "id10-goto-report-subscriptions",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Report Subscriptions",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see Subscriptions",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see Report Groups",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see Report Designer",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": false
            }
          },
          {
            "Name": "ID_11: Goto Cues",
            "Slug": "id11-goto-cues",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Cues",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Cues Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_12: Goto Item Groups",
            "Slug": "id12-goto-item-groups",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Item Groups",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Item Groups Table",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_13: Goto Store Groups",
            "Slug": "id13-goto-store-groups",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Store Groups",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Store Groups Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_14: Goto Data Sources",
            "Slug": "id14-goto-data-sources",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Data Sources",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Data Sources Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_15: Goto Schedule",
            "Slug": "id15-goto-schedule",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Schiedule",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Schedule Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_16 Goto Upload Data",
            "Slug": "id16-goto-upload-data",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Upload Data",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Upload Data Grid",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_17: Goto Report Designer",
            "Slug": "id17-goto-report-designer",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Report Designer",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Report Designer Dektop Panel",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_18: Check Main Menu Options",
            "Slug": "id18-check-main-menu-options",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto the Main Menu",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the Settings Menu",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see the Manage Menu",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see the Data Menu",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_19: Check Settings Menu Options",
            "Slug": "id19-check-settings-menu-options",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Settings",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see Users",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see System Settings",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see Change Log",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see Image Upload",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          },
          {
            "Name": "ID_20: Check Manage Menu Options",
            "Slug": "id20-check-manage-menu-options",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "I login as myself",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "I goto Manage",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see Promotion Execution",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see On Shelf Availability",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see Item Transition",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see Report Subscriptions",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see Cues",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see Item Groups",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I shouldsee Store Groups",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": true,
              "WasSuccessful": true
            }
          }
        ],
        "Result": {
          "WasExecuted": true,
          "WasSuccessful": false
        },
        "Tags": []
      },
      "Result": {
        "WasExecuted": true,
        "WasSuccessful": false
      }
    }
  ],
  "Summary": {
    "Tags": [],
    "Folders": [
      {
        "Folder": "DSM_POC.feature",
        "Total": 2,
        "Passing": 1,
        "Failing": 1,
        "Inconclusive": 0
      },
      {
        "Folder": "Login.feature",
        "Total": 4,
        "Passing": 4,
        "Failing": 0,
        "Inconclusive": 0
      },
      {
        "Folder": "Menu.feature",
        "Total": 20,
        "Passing": 19,
        "Failing": 1,
        "Inconclusive": 0
      }
    ],
    "NotTestedFolders": [
      {
        "Folder": "DSM_POC.feature",
        "Total": 0,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 0
      },
      {
        "Folder": "Login.feature",
        "Total": 0,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 0
      },
      {
        "Folder": "Menu.feature",
        "Total": 0,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 0
      }
    ],
    "Scenarios": {
      "Total": 26,
      "Passing": 24,
      "Failing": 2,
      "Inconclusive": 0
    },
    "Features": {
      "Total": 3,
      "Passing": 1,
      "Failing": 2,
      "Inconclusive": 0
    }
  },
  "Configuration": {
    "GeneratedOn": "11 May 2018 00:41:52"
  }
});